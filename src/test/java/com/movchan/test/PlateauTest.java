package com.movchan.test;

import com.movchan.LongestPlateau.Plateau;
import org.junit.Test;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.function.Executable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class PlateauTest {
    private static Plateau plateau;
    private List<Integer> integers;

    @BeforeAll
    public static void beforeClass(){
        plateau = new Plateau();
        System.out.println("Before Class");
    }
    @BeforeEach
    public void beforeTest(){
        System.out.println("Before test");
    }

    @Test
    public void testForIndex(){
        integers = new ArrayList<>(Arrays.asList(1,2,3,4,5,5,5,5,6,6,8,8,8,7,9));
        assertThrows(IndexOutOfBoundsException.class, ()->integers.get(44));
    }
    @Test
    public void testForNotNull(){
        integers = new ArrayList<>(Arrays.asList(1,2,3,4,5,5,5,5,6,6,8,8,8,7,9));
        assertNotNull(integers);
    }

    @Test
    public void testValue(){
        integers = new ArrayList<>(Arrays.asList(1,2,3,4,5,5,5,5,6,6,8,8,8,7,9));
        assertNotSame(integers.get(2), integers.get(4));
    }

    @Test
    public void testTrueGeneration(){
        integers = new ArrayList<>(Arrays.asList(1,2,3,4,5,5,5,5,6,6,8,8,8,7,9));
        integers.stream().forEach(System.out::print);
        assertNotSame(4, (integers).get(5));
    }

    @AfterEach
    public void afterTest(){
        System.out.println("after");
    }
    @AfterAll
    public static void afterClass(){
        System.out.println("After Class");
    }
}
