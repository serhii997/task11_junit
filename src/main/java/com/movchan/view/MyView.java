package com.movchan.view;

import com.movchan.controller.Controller;
import com.movchan.controller.ControllerImp;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class MyView {
    private final static Logger LOGGER = LogManager.getLogger(MyView.class);
    private static Scanner sc = new Scanner(System.in);

    private Map<String,String> menu;
    private Map<String, Printable> methodMenu;
    private Controller controller;
    private ResourceBundle bundle;
    private Locale locale;
    private List<Integer> plateaus = new ArrayList(Arrays.asList(1,2,3,3,4,5,6,6,6,6,6,7,3,3,3,8,9,9,9,3));


    public MyView() {
        controller = new ControllerImp();
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("MyMenu",locale);
        setMenu();
        methodMenu = new LinkedHashMap<>();
        methodMenu.put("1", this::internationalizeMenuUkraine);
        methodMenu.put("2", this::internationalizeMenuEnglish);
        methodMenu.put("3", (()-> controller.runPlateau(plateaus)));
        methodMenu.put("4", (()-> controller.runMinesweeper(10,15, 5)));
        methodMenu.put("Q", this::exit);
    }

    private void setMenu(){
        menu = new LinkedHashMap<>();
        menu.put("1",bundle.getString("1"));
        menu.put("2",bundle.getString("2"));
        menu.put("3",bundle.getString("3"));
        menu.put("4",bundle.getString("4"));
        menu.put("Q","exit");
    }

    private void internationalizeMenuUkraine(){
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        printMenu();
    }

    private void internationalizeMenuEnglish(){
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        printMenu();
    }

    private void exit(){
        System.exit(0);
    }

    private void printMenu(){
        menu.forEach((key, value) -> System.out.println("`"+key+"` - "+"\t"+value));
    }

    public void run(){
        String input ="";
        printMenu();
        do {
            try {
                input = MyView.sc.next().toLowerCase();
                methodMenu.get(input).print();
            } catch (NullPointerException e) {
                printMenu();
            } catch (Exception e) {
                LOGGER.error(e.getMessage());
            }
        } while (!input.equalsIgnoreCase("Q"));
    }
}
