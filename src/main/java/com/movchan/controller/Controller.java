package com.movchan.controller;

import java.util.List;

public interface Controller {
    void runPlateau(List<Integer> plateau);
    void runMinesweeper(int m, int n, int p);

}
