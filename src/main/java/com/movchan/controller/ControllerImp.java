package com.movchan.controller;

import com.movchan.LongestPlateau.Minesweeper;
import com.movchan.LongestPlateau.Plateau;

import java.util.List;

public class ControllerImp implements Controller{

    @Override
    public void runPlateau(List<Integer> plateau) {
        new Plateau().output(plateau);
    }

    @Override
    public void runMinesweeper(int m, int n, int p) {
        new Minesweeper(m,n,p).run(10, -27, 5);
    }
}
