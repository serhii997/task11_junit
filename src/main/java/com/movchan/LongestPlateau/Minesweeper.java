package com.movchan.LongestPlateau;

import java.util.Random;

public class Minesweeper {
    private int M;
    private int N;
    private int p;
    private boolean[][] sweeper;

    public Minesweeper() {

    }

    public Minesweeper(int m, int n, int p) {
        M = m;
        N = n;
        this.p = p;
        sweeper = new boolean[M][N];
    }

    public void run(int i, int i1, int i2){
        createDesk();
        show();
        System.out.println();
        audit();
    }

    private void createDesk(){
        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                sweeper[i][j] = new Random().nextInt(p) == 0;
            }
        }
    }

    public void show(){
        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                if(sweeper[i][j]){
                    System.out.print("[*]"+" ");
                }else{
                    System.out.print("[|]"+" ");
                }
            }
            System.out.println();
        }
    }

    private void audit(){
        int count = 0;
        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                if(sweeper[i][j]){
                    System.out.print("[*]" +" ");
                }
                else{
                    try {
                        count = countState(i,j);
                        System.out.print("["+count+"]" +" ");
                    }catch (ArrayIndexOutOfBoundsException e){

                    }

                }
            }
            System.out.println();
        }
    }

    private int countState(int i, int j){
        int count = 0;
        try {
            if(sweeper[i-1][j]){
                count++;
            }
        }
        catch (ArrayIndexOutOfBoundsException e){

        }
        try {
            if(sweeper[i][j-1]){
                count++;
            }
        }catch (ArrayIndexOutOfBoundsException e){

        }
        try {
            if(sweeper[i-1][j-1]){
                count++;
            }
        }catch (ArrayIndexOutOfBoundsException e){

        }
        try {
            if(sweeper[i+1][j-1]){
                count++;
            }
        }catch (ArrayIndexOutOfBoundsException e){

        }
        try {
            if(sweeper[i+1][j+1]){
                count++;
            }
        }catch (ArrayIndexOutOfBoundsException e){

        }
        try {
            if(sweeper[i+1][j]){
                count++;
            }
        }catch (ArrayIndexOutOfBoundsException e){

        }
        try {
            if(sweeper[i-1][j+1]){
                count++;
            }
        }catch (ArrayIndexOutOfBoundsException e){

        }
        try {
            if(sweeper[i][j+1]){
                count++;
            }
        }catch (ArrayIndexOutOfBoundsException e){

        }
        return count;
    }

}
