package com.movchan.LongestPlateau;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class Plateau {

        private final static Logger LOGGER = LogManager.getLogger(Plateau.class);

        public List<MyNumber> getNumbers(List<Integer> integerList) {
            List<MyNumber> countRepeatIntegers = new ArrayList<>();
            MyNumber previous = new MyNumber();
            for (int i = 0; i < integerList.size(); i++) {
                if (!integerList.get(i).equals(previous.getValue())) {
                    countRepeatIntegers.add(new MyNumber(integerList.get(i), i, i+1));
                } else {
                    countRepeatIntegers.get(countRepeatIntegers.size() - 1).setLastNumber(i+1);
                }
                previous = countRepeatIntegers.get(countRepeatIntegers.size() - 1);
            }
            return countRepeatIntegers;
        }

        public void output(List<Integer> plateaus){
            getNumbers(plateaus).forEach(System.out::println);
            getNumbers(plateaus)
                    .stream()
                    .sorted(Comparator.comparingInt(numberArray -> numberArray.getFirstNumber() - numberArray.getLastNumber()))
                    .limit(1)
                    .forEach(numberArray -> System.out.println("the longest contiguous sequence " + numberArray));
        }



        class MyNumber{
            private int value;
            private int firstNumber;
            private int lastNumber;

            public MyNumber() {
            }

            public MyNumber(int value, int firstNumber, int lastNumber) {
                this.value = value;
                this.firstNumber = firstNumber;
                this.lastNumber = lastNumber;
            }

            public int getValue() {
                return value;
            }

            public void setValue(int value) {
                this.value = value;
            }

            public int getFirstNumber() {
                return firstNumber;
            }

            public void setFirstNumber(int firstNumber) {
                this.firstNumber = firstNumber;
            }

            public int getLastNumber() {
                return lastNumber;
            }

            public void setLastNumber(int lastNumber) {
                this.lastNumber = lastNumber;
            }


            @Override
            public String toString() {
                return "MyNumber{" +
                        "value=" + value +
                        ", firstNumber=" + firstNumber +
                        ", lastNumber=" + lastNumber +
                        '}';
            }
        }
}
